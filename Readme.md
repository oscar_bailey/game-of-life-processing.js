Conway's Game of Life
=====================

Written in [processing](http://processing.org) and Javascript.

To see it in action:
[oscarbailey.co.uk/life/](http://oscarbailey.co.uk/life/)

To use:

* Upload files to your webserver

* Upload processing.min.js

* Change the Javascript var searchStr (found in index.html) to the last part of your URL


