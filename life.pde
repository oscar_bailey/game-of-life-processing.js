int[][] cells;
int[][] latestCells;
int cols = 100;
int rows = 100;
int cWidth = 6;
int cHeight = 6;
int windowWidth = cWidth * cols;
int windowHeight = cHeight * rows;
int bgColor = 50;
int desiredFrameRate = 10;
boolean myFocus = false;
boolean kaleidoscope = false;
color kaleidoscopeC = color(0,0,0);
int tick = 0;

void setup() {
  background(bgColor);
  size(600, 600);
  frameRate(desiredFrameRate);
  running = false;
  cells = new int[cols][rows];
  latestCells = new int[cols][rows];
  for ( int i = 0; i < cols; i++){
    for (int j = 0; j < rows; j++){
      cells[i][j] = 0;
      latestCells[i][j] = 0;
    }
  }
  string code = getURL();
  loadCells(code);
  drawCells();
}

void draw() {
  tick += 0.1;
  if (tick > 6.28) { tick=0; }
  if (running) {
    background(bgColor);
    step();
    textAlign(CENTER, TOP);
    text("Running", windowWidth/2 , 70);
  }
  if (!focused) {
    textAlign(CENTER, TOP);
    text("Click inside the window!", windowWidth/2 , windowHeight/3);
    myFocus = false;
  }
  if (kaleidoscope) {
    kaleidoscopeC = color((sin(tick)+1)*120, (cos(tick)+1)*120, (sin(tick/2)+1)*120);
    fill(kaleidoscopeC);
  }
  if (!kaleidoscope) {
    fill(255);
  }
}

void step() {
  for ( int i = 0; i < cols; i++){
    for (int j = 0; j < rows; j++){
      int s = 0;
      for (int p = i-1; p <= i+1; p++){
        for (int q = j-1; q <= j+1; q++){
          int newP = p;
          int newQ = q;
          if (p<0){
            newP=p+100;
          } else if (p>=100){
            newP=p%100;
          }
          if (q<0){
            newQ=q+100;
          } else if (q>=100){
            newQ=q%100;
          }
          s = s+cells[newP][newQ];
        }
      }
      s = s-cells[i][j];
      if (s==3 || (s+cells[i][j])==3){
        latestCells[i][j] = 1;
        rect(i*cWidth, j*cHeight, cWidth, cHeight);
      } else {
        latestCells[i][j] = 0;
      }
    }
  }
  updateCells();
}

void keyPressed() {
  if (key == 's'){
    if (running) {
      running = false;
      drawCells();
    } else {
      running = true;
    }
  } else if (key == 'c'){
    clearCells();
  } else if (key == 'r'){
    string code = getBox();
    loadCells(code);
    drawCells();
  } else if (key == 'u'){
    string code = getURL();
    loadCells(code);
    drawCells();
  } else if (key == 'k'){
    if (kaleidoscope) {
      kaleidoscope = false;
    } else {
      kaleidoscope = true;
    }
  } else if (key == CODED){
    if (keyCode == UP){
      desiredFrameRate += 2;
      frameRate(desiredFrameRate);
    } else if (keyCode == DOWN){
      desiredFrameRate -= 2;
      if (desiredFrameRate <= 0) {
        desiredFrameRate = 2;
        running = false;
        drawCells();
      } else {
        frameRate(desiredFrameRate);
      }
    } else if (keyCode == RIGHT){
      background(bgColor);
      step();
    }
  }
}

void drawCells() {
  background(bgColor);
  for ( int i = 0; i < cols; i++){
    for (int j = 0; j < rows; j++){
      if (cells[i][j] == 1){
        rect(i*cWidth, j*cHeight, cWidth, cHeight);
      }
    }
  }
}

void updateCells() {
  for ( int i = 0; i < cols; i++){
    for (int j = 0; j < rows; j++){
      cells[i][j] = latestCells[i][j];
    }
  }
}

void clearCells() {
  for ( int i = 0; i < cols; i++){
    for (int j = 0; j < rows; j++){
      cells[i][j] = 0;
    }
  }
  drawCells();
}

void outputCells() {
  string outputStr = "";
  int[] offset = new int[2];
  for ( int i = 0; i < cols; i++){
    for (int j = 0; j < rows; j++){
      if (cells[i][j] == 1) {
        if (outputStr.length() == 0) {
          // Create Offset
          offset = {i, j};
          outputStr += i + "." + j + "+0.0";
        } else {
          int p = i - offset[0];
          int q = j - offset[1];
          outputStr += "," + p + "." + q;
        }
        
      }
    }
  }
  outputCode(outputStr);
}

void loadCells(cellStr) {
  // Get Offset.
  int plus = cellStr.indexOf("+");
  if (plus == -1){
    return
  }
  string offsetStr = cellStr.substring(0,plus);
  int[] offset = getXY(offsetStr);

  cellStr = cellStr.substring(plus+1)

  clearCells();
  int comma;
  string inputString;
  int[] coords;
  boolean quit = false;
  while (true) {
    if (quit) {break;}
    comma = cellStr.indexOf(",");
    if (comma == -1){
      quit = true;
      inputString = cellStr;
    } else {
      inputString = cellStr.substring(0,comma);
    }
    coords = getXY(inputString);
    cells[offset[0]+coords[0]][offset[1]+coords[1]] = 1;
    cellStr = cellStr.substring(comma+1);
  }

}

void getXY(encodedCoord) {
  string cStr = encodedCoord;
  int point = cStr.indexOf(".");
  string iStr = cStr.substring(0,point);
  string jStr = cStr.substring(point+1);
  int[] coords = {parseInt(iStr), parseInt(jStr)};
  return coords;
}

void mouseClicked() {
  if (myFocus && !running){
    int i = int(mouseX / cWidth);
    int j = int(mouseY / cHeight);
    
    if (cells[i][j] == 0){
      cells[i][j] = 1;
    } else {
      cells[i][j] = 0;
    }
    drawCells();
    outputCells();
  }
  myFocus = true;
  
}

void mouseMoved() {
  if (myFocus && !running) {
    int i = int(mouseX / cWidth);
    int j = int(mouseY / cHeight);
    
    drawCells();
    fill(100);
    rect(i*cWidth, j*cHeight, cWidth, cHeight);
    fill(255);
  }
}

void mouseDragged() {
  if (myFocus && !running) {
    int i = int(mouseX / cWidth);
    int j = int(mouseY / cHeight);
    
    cells[i][j] = 1;
    drawCells();
    outputCells();
  }
} 

